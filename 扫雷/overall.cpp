#include "overall.h"

Ovreall::Ovreall()
{
}

/*
	只调用一次
*/
Ovreall::Ovreall(int row,int col,int imageSize)
{
	this->row = row;
	this->col = col;
	this->imageSize = imageSize;
}



Ovreall::~Ovreall()
{
}


/*
	游戏结束调用初始化
*/
void Ovreall::reset()
{
	Sleep(3000);
	IMAGE over;
	loadimage(&over, "../resouce/gameover.jpeg");
	putimage(90,90,&over);

	Sleep(3000);
	for (int i = 0; i < this->row; i++)
	{
		vector<int> cl;
		for (int j = 0; j < this->col; j++)
		{
			cl.push_back(rand() % (0, 5));
		}
		this->ovMap.push_back(cl);
	}
	initgraph(543, 540);
	loadimage(NULL, "../resouce/p.jpg");
}

/*
	putimage(scol*imageSize, srow*imageSize, &this->imageSzro);
	srow = direSum++;
	scol = direSum++; 
	只显示白图，白图之后是数字的一律没有显示
*/
bool Ovreall::isExistAndClick(int y, int x)
{
	int row = y / imageSize;
	int col = x / imageSize;
	/*
	控制方向
	1 ++
	2 +
	3 +-
	4  -
	5 --
	6 - 
	7 -+
	8  +
	*/
	int sum = 1;

	int srow;
	int scol;
	//定义控制方向值
	int direSum = 1;

	if (ovMap[row][col] == IMAGE_SZRO)
	{
		putimage(col*imageSize, row*imageSize, &this->imageSzro);

		//点击到空位置就将空位置周围的数字显示出来
		for (int i = 1; i <= 8; i++)
		{
			srow = 0;
			scol = 0;
			if (sum == 1)
			{
				srow = row + direSum;
				scol = col + direSum;
			}
			else if (sum == 2) 
			{
				srow = row + direSum;
				scol = col;
			}
			else if (sum == 3)
			{
				srow = row + direSum;
				scol = col - direSum;
			}
			else if (sum == 4)
			{
				srow = row;
				scol = col - direSum;
			}
			else if (sum == 5)
			{
				srow = row - direSum;
				scol = col - direSum;
			}
			else if (sum == 6)
			{
				srow = row - direSum;
				scol = col;
			}
			else if (sum == 7)
			{
				srow = row - direSum;
				scol = col + direSum;
			}
			else if (sum == 8)
			{
				srow = row;
				scol = col + direSum;
			}
			else
			{
				break;
			}

			for (int dire = 1; dire < 100; dire++)
			{
				if (srow < this->row && srow >= 0 &&
					scol < this->col && scol >= 0 &&
					ovMap[srow][scol] == IMAGE_SZRO)
				{
					putimage(scol*imageSize, srow*imageSize, &this->imageSzro);
					//srow = direSum++;
					//scol = direSum++;
					
					direSum++;
					break;
				}
				else if (srow < this->row && srow >= 0 &&
					scol < this->col && scol >= 0 &&
					ovMap[srow][scol] == IMAGE_ONE)
				{
					putimage(scol*imageSize, srow*imageSize, &this->imageOne);
					sum++;
					direSum = 1;
					break;
				}
				else if (srow < this->row && srow >= 0 &&
					scol < this->col && scol >= 0 &&
					ovMap[srow][scol] == IMAGE_TWO)
				{
					putimage(scol*imageSize, srow*imageSize, &this->imageTwo);
					sum++;
					direSum = 1;
					break;
				}
				else if (srow < this->row && srow >= 0 &&
					scol < this->col && scol >= 0 &&
					ovMap[srow][scol] == IMAGE_THREE)
				{
					putimage(scol*imageSize, srow*imageSize, &this->imageThree);
					sum++;
					direSum = 1;
					break;
				}
				else if (srow < this->row && srow >= 0 &&
					scol < this->col && scol >= 0 &&
					ovMap[srow][scol] == IMAGE_FOUR)
				{
					sum++;
					direSum = 1;
					break;
				}
			}
		}

	}
	else if (ovMap[row][col] == IMAGE_ONE)
	{
		putimage(col*imageSize, row*imageSize, &this->imageOne);
	}
	else if (ovMap[row][col] == IMAGE_TWO)
	{
		putimage(col*imageSize, row*imageSize, &this->imageTwo);
	}
	else if (ovMap[row][col] == IMAGE_THREE)
	{
		putimage(col*imageSize, row*imageSize, &this->imageThree);
	}
	else if (ovMap[row][col] == IMAGE_FOUR)
	{
		for (int i = 0; i < this->row; i++)
		{
			for (int j = 0; j < this->col; j++)
			{
				if (ovMap[i][j] == IMAGE_FOUR)
				{
					putimage(j*imageSize, i*imageSize, &this->imageFour);
				}
			}
		}
		//重置游戏
		reset();
	}
	return false;
}

void Ovreall::init()
{
	/*初始化扫雷数据*/
	for (int i = 0; i < this->row; i++)
	{
		vector<int> cl;
		for (int j = 0; j < this->col; j++)
		{
			cl.push_back(rand() % (0, 5));
		}
		this->ovMap.push_back(cl);
	}


	initgraph(543, 540);
	loadimage(NULL, "../resouce/p.jpg");
	loadimage(&this->imageSzro, "../resouce/null.jpg");
	loadimage(&this->imageOne, "../resouce/1.jpg");
	loadimage(&this->imageTwo, "../resouce/2.jpg");
	loadimage(&this->imageThree, "../resouce/3.jpg");
	loadimage(&this->imageFour, "../resouce/mine.jpg");
}

