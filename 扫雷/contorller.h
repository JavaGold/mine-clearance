#pragma once
#include <stdio.h>
#include "overall.h"

class Controller
{
public:
	Controller(Ovreall* ovreall);
	~Controller();

	void go();

private:
	Ovreall* ov;
};

